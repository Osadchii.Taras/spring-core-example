package com.epam.rd.spring.tickets.repository.inmemory;

import com.epam.rd.spring.tickets.model.Ticket;

import java.util.List;

public interface TicketRepository {
    Ticket findById(String id);
    void addTicket(Ticket ticket);
    List<Ticket> findAll();
}
