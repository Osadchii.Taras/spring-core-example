package com.epam.rd.spring.tickets.repository.inmemory.impl;

import com.epam.rd.spring.tickets.model.Ticket;
import com.epam.rd.spring.tickets.repository.inmemory.TicketRepository;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class TicketRepositoryImpl implements TicketRepository {

    private List<Ticket> tickets;

    @PostConstruct
    private void init() {
        tickets = new ArrayList<>();
        tickets.add(Ticket.builder()
                .id("new id")
                .owner("Taras")
                .price(100)
                .build());
    }


    @Override
    public Ticket findById(String id) {
        return tickets.stream()
                .filter(ticket -> ticket.getId().equals(id))
                .findAny().orElseThrow();
    }

    @Override
    public void addTicket(Ticket ticket) {
        tickets.add(Ticket.builder()
                .id(ticket.getId())
                .price(ticket.getPrice())
                .owner(ticket.getOwner())
                .build());
    }

    @Override
    public List<Ticket> findAll() {
        return tickets;
    }
}
