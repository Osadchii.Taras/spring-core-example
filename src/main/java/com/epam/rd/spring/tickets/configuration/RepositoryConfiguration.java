package com.epam.rd.spring.tickets.configuration;

import com.epam.rd.spring.tickets.repository.inmemory.TicketRepository;
import com.epam.rd.spring.tickets.repository.inmemory.impl.TicketRepositoryImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class RepositoryConfiguration {

//    @Bean
//    @Primary
//    public TicketRepository ticketRepository() {
//        return new TicketRepositoryImpl();
//    }
}
