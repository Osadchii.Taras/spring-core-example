package com.epam.rd.spring.tickets.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
@Data
@Builder
public class Ticket {
    private String id;
    private Integer price;
    private String owner;
}
