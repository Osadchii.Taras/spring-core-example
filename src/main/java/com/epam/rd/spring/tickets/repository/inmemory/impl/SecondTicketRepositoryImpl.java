package com.epam.rd.spring.tickets.repository.inmemory.impl;

import com.epam.rd.spring.tickets.model.Ticket;
import com.epam.rd.spring.tickets.repository.inmemory.TicketRepository;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.List;

@Component("ticketRepository")
@Scope("prototype")
public class SecondTicketRepositoryImpl implements TicketRepository, BeanPostProcessor {

    private List<Ticket> tickets;

    @PostConstruct
    private void init() {
        tickets = new ArrayList<>();
        tickets.add(Ticket.builder()
                .id("new id")
                .owner("Taras")
                .price(100)
                .build());
    }

    @PreDestroy
    private void destroy() {
        //do somth
    }


    @Override
    public Ticket findById(String id) {
        return tickets.stream()
                .filter(ticket -> ticket.getId().equals(id))
                .findAny().orElseThrow();
    }

    @Override
    public void addTicket(Ticket ticket) {
        tickets.add(Ticket.builder()
                .id(ticket.getId())
                .price(ticket.getPrice())
                .owner(ticket.getOwner())
                .build());
    }

    @Override
    public List<Ticket> findAll() {
        return tickets;
    }
}
