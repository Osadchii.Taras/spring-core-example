package com.epam.rd.spring.tickets.controller;

import com.epam.rd.spring.tickets.model.Ticket;
import com.epam.rd.spring.tickets.repository.inmemory.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/ticket")
public class TicketController {

    @Autowired
    @Qualifier("ticketRepository")
    private TicketRepository repository;


    @Autowired
    private List<TicketRepository> repositories;


    @GetMapping("/{id}")
    public Ticket findById(@PathVariable String id) {
        return repository.findById(id);
    }

    @PostMapping
    public void addTicket(@RequestBody Ticket ticket) {
        repository.addTicket(ticket);
    }

    @GetMapping
    public List<List<Ticket>> findAll() {
        return repositories.stream()
                .map(TicketRepository::findAll)
                .collect(Collectors.toList());
//        return repository.findAll();
    }
}
